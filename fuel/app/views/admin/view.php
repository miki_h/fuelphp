<!DOCTYPE html>
<html>
<head>
	<meta charset="UTF-8">
	<title>一覧画面</title>
	<style>
		body{
			margin: 40px;
		}
		ul li {
		    width: 15%;
		    float: left;
		    margin-right: 5%;
		    list-style: none;
		}
		ul li:last-child {
		    margin-right: 0%;
		}
		ul li img{
		    width: 100%;
		}
	</style>
	
</head>
<body>
	一覧画面
	<?php echo Html::anchor('admin/logout', 'ログアウト'); ?>
	<br>
	<?php echo Html::anchor('admin/upload', 'アップロード'); ?>

	<div>
		<ul>
		<?php foreach ($images as $img): ?>
			<li>
				<?php echo Asset::img($img['file_name']); ?>
				<span class="votes"><?php echo $img['votes']; ?></span>
			</li>
		<?php endforeach; ?>
		</ul>
	</div>
</body>
</html>
