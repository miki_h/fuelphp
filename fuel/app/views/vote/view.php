<!DOCTYPE html>
<html>
<head>
	<meta charset="UTF-8">
	<title>一覧画面</title>
	<style>
		body{
			margin: 40px;
		}
		.container{
			width: 80%;
			margin: 0 auto;
		}
		ul{
			margin:0 0 10px;
		    display:-webkit-box;
		    display:-moz-box;
		    display:-ms-box;
		    display:-webkit-flexbox;
		    display:-moz-flexbox;
		    display:-ms-flexbox;
		    display:-webkit-flex;
		    display:-moz-flex;
		    display:-ms-flex;
		    display:flex;
		    -webkit-box-lines:multiple;
		    -moz-box-lines:multiple;
		    -webkit-flex-wrap:wrap;
		    -moz-flex-wrap:wrap;
		    -ms-flex-wrap:wrap;
		    flex-wrap:wrap;
		}
		ul li {
		    width: 15%;
		    float: left;
		    margin-right: 5%;
		    list-style: none;
		    margin:0 10px 10px 0;
		    padding:10px;
		    border:1px solid #aaa;
		    width:300px;
		    list-style:none;
		    position: relative;
		}
		ul li div.mode{
			position: absolute;
			bottom: 0;
			right: 0;
		}
		ul li:last-child {
		    margin-right: 0%;
		}
		ul li img{
		    width: 100%;
		    padding-bottom: 20px;
		}
	</style>
	<script src="https://code.jquery.com/jquery-2.2.4.min.js"   integrity="sha256-BbhdlvQf/xTY9gja0Dq3HiwQF8LaCRTXxZKRutelT44="   crossorigin="anonymous"></script>
	<script>
		$(function(){
			$('input.vote').on('click', function(){
				var id = $(this).data('id');
				$.ajax({
					url: "<?php echo Uri::create('api/vote.json'); ?>",
					type: "POST",
					data: {id: id},
					dataType: "json"
				}).done(function(data){
					alert(data.message);
					location.reload();
				}).fail(function(data){
					alert('処理に失敗しました。');
				});
			})
		})
	</script>
	
</head>
<body>
	一覧画面
	<?php echo Html::anchor('vote/logout', 'ログアウト'); ?>

	<div class="container">
		<ul>
		<?php foreach ($images as $img): ?>
			<li>
				<?php echo Asset::img($img['file_name']); ?>
				<div class="mode">
					<span class="votes"><?php echo $img['votes']; ?></span>
					<input type="button" class="vote" data-id="<?php echo $img['id']; ?>" value="投票する">
				</div>
			</li>
		<?php endforeach; ?>
		</ul>
	</div>
</body>
</html>
