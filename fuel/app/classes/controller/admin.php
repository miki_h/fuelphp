<?php
class Controller_Admin extends Controller {

	public function action_login(){
		if(Input::post('id') == 'admin' && Input::post('password') == 'pass'){
				// echo 'ログインOK';
			Session::set('admin_login', true);
			Response::redirect('admin/view');
		}
		if(Session::get('admin_login') == true){
			Response::redirect('admin/view');
		}
		return Response::forge(View::forge('admin/login'));
	}

	public function action_view(){
		if(Session::get('admin_login') != true){
			Response::redirect('admin/login');
		}

		// データをロード
		$images = Model_Image::find('all');
		$data = array('images' => $images);
		
		return Response::forge(View::forge('admin/view', $data));
	}

	public function action_logout(){
		//セッションの情報を消す
		Session::delete('admin_login', true);
		//ログイン画面に戻る
		Response::redirect('admin/login');
	}

	public function action_upload(){
		if(Session::get('admin_login') != true){
			Response::redirect('admin/login');
		}

		if(Input::file('image')){

			$image = Input::file('image');
			move_uploaded_file($image['tmp_name'], 'assets/img/'. $image['name']);

			$model_image = new Model_Image();
			$model_image->file_name = $image['name'];
			$model_image->info = '';
			$model_image->votes = 0;
			$model_image->save();

		}

		//ログイン画面に戻る
		return Response::forge(View::forge('admin/upload'));
	}
}