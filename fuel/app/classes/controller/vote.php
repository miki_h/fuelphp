<?php
class Controller_Vote extends Controller {

	public function action_login(){
		if(Input::post('id') != ''){
			if(Auth::login(Input::post('id'), Input::post('password'))){
				Response::redirect('vote/view');
			}else{
				echo 'ユーザーまたはパスワードが違います';
			}
		}
		return Response::forge(View::forge('vote/login'));
	}

	public function action_logout(){
		Auth::logout();
		//ログイン画面に戻る
		Response::redirect('vote/login');
	}

	public function action_view(){
		if(! Auth::Check()){
			Response::redirect('vote/login');
		}

		// データをロード
		$images = Model_Image::find('all');
		$data = array('images' => $images);
		return Response::forge(View::forge('vote/view', $data));
	}

}